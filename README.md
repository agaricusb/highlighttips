HighlightTips

A simple client mod for Minecraft to show the block name highlighted at your cursor tip:

![Screenshot](http://i.imgur.com/sv2NqEn.png "Screenshot")

The block ID and metadata is shown, followed by the block name, the item name dropped, and finally the item damage value.

Latest builds for download at Buildhive: [![Build Status](https://buildhive.cloudbees.com/job/agaricusb/job/HighlightTips/badge/icon)](https://buildhive.cloudbees.com/job/agaricusb/job/HighlightTips/)


See also:

* [Not Enough Items](http://www.minecraftforum.net/topic/909223-14715-smp-chickenbones-mods/) - original inspiration for highlight tips
* [bspkrs' Mods](http://www.minecraftforum.net/topic/1114612-152-bspkrs-mods-armorstatushud-v17-directionhud-v113-statuseffecthud-v110/), [Ingame Info](http://www.minecraftforum.net/topic/1009577-152-daftpvfs-mods-treecapitator-ingameinfo-crystalwing-startinginv-floatingruins/) - shows other useful information


